package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

func main() {

	router := gin.Default()

	v1 := router.Group("/api/v1/test")
	{
		v1.GET("/", helloFunction)
	}
	router.Run()

}

func helloFunction(c *gin.Context) {

	c.JSON(http.StatusOK, gin.H{"msg": "Hello world!"})
}
